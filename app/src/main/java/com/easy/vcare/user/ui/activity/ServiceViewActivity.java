package com.easy.vcare.user.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easy.vcare.user.BaseActivity;
import com.easy.vcare.user.R;
import com.easy.vcare.user.adapter.ServiceSelectionAdapter;
import com.easy.vcare.user.adapter.ServicesViewAdapter;
import com.easy.vcare.user.utils.RecyclerTouchListener;

import java.util.ArrayList;

public class ServiceViewActivity extends BaseActivity {

    private TextView mTitle;
    private ImageView mImage;
    private RecyclerView mRecyclerviewServices;
    RecyclerView.LayoutManager mLayoutManager;
    ServicesViewAdapter servicesViewAdapter;

    private ArrayList<String> mNames_Assistance = new ArrayList<>();
    private ArrayList<Integer> mImageUrls_Assistance = new ArrayList<Integer>();

    private ArrayList<String> mNames_Ride = new ArrayList<>();
    private ArrayList<Integer> mImageUrls_Ride = new ArrayList<Integer>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        statusbarTheme();
        setContentView(R.layout.activity_services_view);
        initView();

    }

    private void initView() {
        mTitle = (TextView) findViewById(R.id.title);
        mImage = (ImageView) findViewById(R.id.image);
        mRecyclerviewServices = (RecyclerView) findViewById(R.id.services_recyclerview);

        viewData();

    }

    public void viewData()
    {

        mLayoutManager = new LinearLayoutManager(ServiceViewActivity.this);
        mRecyclerviewServices.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(mRecyclerviewServices, false);
        mRecyclerviewServices.setHasFixedSize(true);
        mRecyclerviewServices.setItemViewCacheSize(20);
        mRecyclerviewServices.setDrawingCacheEnabled(true);
        mRecyclerviewServices.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        switch (ServiceSelectionActivity.getFragment_value()) {
            case 0:
                mTitle.setText("Car Assistance");
                mImage.setImageResource(R.drawable.carassit);
                getCarAssitanceData();
                break;

            case 1:
                mTitle.setText("Ride Book");
                mImage.setImageResource(R.drawable.bookride);
                getRideData();
                break;
        }
    }

    private void getCarAssitanceData() {


        mImageUrls_Assistance.add(R.drawable.fueling);
        mNames_Assistance.add("Car Refueling");

        mImageUrls_Assistance.add(R.drawable.towing);
        mNames_Assistance.add("Tow Car");

        mImageUrls_Assistance.add(R.drawable.biketowing);
        mNames_Assistance.add("Tow Bike");

        mImageUrls_Assistance.add(R.drawable.battery);
        mNames_Assistance.add("Battery Recharge");

        servicesViewAdapter = new ServicesViewAdapter(ServiceViewActivity.this, mNames_Assistance, mImageUrls_Assistance);
        mRecyclerviewServices.setLayoutManager(mLayoutManager);
        mRecyclerviewServices.setAdapter(servicesViewAdapter);

        mRecyclerviewServices.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerviewServices, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {


                /*assistance_service_value = position;
                Toast.makeText(ServiceViewActivity.this, ""+position, Toast.LENGTH_SHORT).show();*/
                mainIntent = new Intent(ServiceViewActivity.this, MainActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                ServiceViewActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }

    private void getRideData() {

        mImageUrls_Ride.add(R.drawable.economy);
        mNames_Ride.add("Economy");

        mImageUrls_Ride.add(R.drawable.economy);
        mNames_Ride.add("Economy Plus");

        mImageUrls_Ride.add(R.drawable.business);
        mNames_Ride.add("Business");

        servicesViewAdapter = new ServicesViewAdapter(ServiceViewActivity.this, mNames_Ride, mImageUrls_Ride);
        mRecyclerviewServices.setLayoutManager(mLayoutManager);
        mRecyclerviewServices.setAdapter(servicesViewAdapter);

        mRecyclerviewServices.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerviewServices, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {


                ride_service_value = position;
                Toast.makeText(ServiceViewActivity.this, ""+position, Toast.LENGTH_SHORT).show();
               /* mainIntent = new Intent(ServiceViewActivity.this, MainActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                ServiceViewActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);*/
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intentTransition();
    }

}