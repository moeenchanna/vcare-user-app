package com.easy.vcare.user.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.easy.vcare.user.BaseActivity;
import com.easy.vcare.user.R;

public class RideHistoryActivity extends BaseActivity implements View.OnClickListener {

    public ImageView mBackBtn;
    public TextView mTitle;
    private RecyclerView mRecyclerviewRide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_history);
        initView();
    }

    private void initView() {
        mBackBtn = (ImageView) findViewById(R.id.btn_back);
        mTitle = (TextView) findViewById(R.id.title);
        mRecyclerviewRide = (RecyclerView) findViewById(R.id.ride_recyclerview);
        mBackBtn.setOnClickListener(this);
        baseToolbar(getString(R.string.ride_history));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                // TODO 20/07/21
                break;
            default:
                break;
        }
    }
}