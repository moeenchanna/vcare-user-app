package com.easy.vcare.user.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.easy.vcare.user.BaseActivity;
import com.easy.vcare.user.R;
import com.easy.vcare.user.adapter.ServiceSelectionAdapter;
import com.easy.vcare.user.utils.RecyclerTouchListener;

import java.util.ArrayList;

public class ServiceSelectionActivity extends BaseActivity {

    private RecyclerView mRecyclerviewServic;
    ServiceSelectionAdapter serviceSelectionAdapter;

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<Integer> mImageUrls = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        statusbarTheme();

        setContentView(R.layout.activity_service_selection);
        initView();
    }

    private void initView() {
        mRecyclerviewServic = (RecyclerView) findViewById(R.id.servic_recyclerview);

        mImageUrls.add(R.drawable.carassit);
        mNames.add("Car Assistance");

        mImageUrls.add(R.drawable.bookride);
        mNames.add("Ride Booking");

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ServiceSelectionActivity.this);

        serviceSelectionAdapter = new ServiceSelectionAdapter(ServiceSelectionActivity.this, mNames, mImageUrls);
        mRecyclerviewServic.setLayoutManager(mLayoutManager);
        mRecyclerviewServic.setAdapter(serviceSelectionAdapter);
        mRecyclerviewServic.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(mRecyclerviewServic, false);
        mRecyclerviewServic.setHasFixedSize(true);
        mRecyclerviewServic.setItemViewCacheSize(20);
        mRecyclerviewServic.setDrawingCacheEnabled(true);
        mRecyclerviewServic.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        mRecyclerviewServic.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), mRecyclerviewServic, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {


                fragment_value = position;
                mainIntent = new Intent(ServiceSelectionActivity.this, ServiceViewActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                ServiceSelectionActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intentTransition();
    }
}