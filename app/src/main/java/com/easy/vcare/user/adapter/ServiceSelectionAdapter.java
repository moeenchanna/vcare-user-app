package com.easy.vcare.user.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.easy.vcare.user.R;

import java.util.ArrayList;

public class ServiceSelectionAdapter extends RecyclerView.Adapter<ServiceSelectionAdapter.ViewHolder>{

    private static final String TAG = "ServiceSelectionAdapter";

    private ArrayList<String> mImageNames = new ArrayList<>();
    private ArrayList<Integer> mImages = new ArrayList<>();
    private Context mContext;

    public ServiceSelectionAdapter(Context context, ArrayList<String> imageNames, ArrayList<Integer> images ) {
        mImageNames = imageNames;
        mImages = images;
        mContext = context;
    }

    @Override
    public ServiceSelectionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View rootView = inflater.inflate(R.layout.recycler_services_layout, parent, false);
        return new ServiceSelectionAdapter.ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(ServiceSelectionAdapter.ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        /* if (mImages == null || mImages.isEmpty())
            return;
        final int P = position % mImages.size();
        String url = mImages.get(P);
        Glide.with(mContext).load(url).into(holder.image);*/
        holder.imageName.setText(mImageNames.get(position));
        holder.image.setImageResource(mImages.get(position));

    }

    @Override
    public int getItemCount() {
        return mImageNames.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView image;
        TextView imageName;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.service_image);
            imageName = itemView.findViewById(R.id.service_name);
        }
    }
}