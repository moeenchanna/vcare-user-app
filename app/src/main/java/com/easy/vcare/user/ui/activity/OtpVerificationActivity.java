package com.easy.vcare.user.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.easy.vcare.user.BaseActivity;
import com.easy.vcare.user.R;

public class OtpVerificationActivity extends BaseActivity implements View.OnClickListener {

    private TextView mButtonResend;
    private Button mButtonVerify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        statusbarTheme();
        setContentView(R.layout.activity_otp_verification);
        initView();
    }

    private void initView() {
        mButtonResend = (TextView) findViewById(R.id.resend_button);
        mButtonResend.setOnClickListener(this);
        mButtonVerify = (Button) findViewById(R.id.verify_button);
        mButtonVerify.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.resend_button:
                // TODO 20/06/26
                Toast.makeText(this, "Resend", Toast.LENGTH_SHORT).show();

                break;
            case R.id.verify_button:
                // TODO 20/06/26

                mainIntent = new Intent(OtpVerificationActivity.this,ServiceSelectionActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                OtpVerificationActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intentTransition();
    }
}