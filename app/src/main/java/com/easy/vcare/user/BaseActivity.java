package com.easy.vcare.user;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;

public class BaseActivity extends AppCompatActivity {


    /**
     * Permissions Properties
     **/
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5445;

    /**
     * Static Members
     **/
    public static int fragment_value;
    public static int ride_service_value;
    public static int assistance_service_value;
    /**
     * Duration of wait
     **/
    public final int SPLASH_DISPLAY_LENGTH = 1000;

    /**
     * Map Properties
     **/
    public GoogleMap googleMap;
    public FusedLocationProviderClient fusedLocationProviderClient;
    public Marker currentLocationMarker;
    public Location currentLocation;

    /**
     * Intent Properties
     **/
    public boolean firstTimeFlag = true;
    public Intent mainIntent;

    /**
     * Toolbar Properties
     **/
    public TextView mTitle;
    public void baseToolbar(String title)
    {
        mTitle.setText(title);
    }

    /**
     * Static Members Properties
     **/
    public static int getFragment_value() {
        return fragment_value;
    }

    public static int getRide_service_value() {
        return ride_service_value;
    }

    public static int getAssistance_service_value() {
        return assistance_service_value;
    }


    /**
     * Themes Properties
     **/
    public void statusbarTheme() {
        //Change status bar color
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.white));
        }

    }

    public void mapstatusbarTheme() {
        //Change status bar color
        if (Build.VERSION.SDK_INT >= 21) {
            Window w = getWindow();
            w.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            w.setStatusBarColor(0x00000000);
        }

    }

    public void intentTransition() {
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    /**
     * Map Styling Theme
     **/
    public void mapStyle() {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.uber_style));

            if (!success) {
                Log.e("MapsActivityRaw", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MapsActivityRaw", "Can't find style.", e);
        }
    }



}
