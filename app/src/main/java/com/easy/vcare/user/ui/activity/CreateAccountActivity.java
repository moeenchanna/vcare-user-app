package com.easy.vcare.user.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.easy.vcare.user.BaseActivity;
import com.easy.vcare.user.R;

public class CreateAccountActivity extends BaseActivity implements View.OnClickListener {


    private CardView mNextButtonCard;
    private Button mButtonNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        statusbarTheme();
        setContentView(R.layout.activity_create_account);
        initView();


    }

    private void initView() {
        mButtonNext = (Button) findViewById(R.id.next_button);
        mButtonNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next_button:
                // TODO 20/06/26

                mainIntent = new Intent(CreateAccountActivity.this,OtpVerificationActivity.class);
                mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NO_ANIMATION);
                CreateAccountActivity.this.startActivity(mainIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intentTransition();
    }
}